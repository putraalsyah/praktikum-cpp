/*
   Nama Program : masukAbj.cc
   Tgl buat     : 10 Oktober 2023
   Deskripsi    : memeriksa inputan
*/

#include <iostream>
#include <stdlib.h>

using namespace std;

int main()
{
  system("clear");

  char A = ' ';

  cout << "Masukkan Suatu Karakter : "; cin >> A;

  if ((A >= 'A') and (A <= 'Z'))
  {
    cout << "Anda menekan huruf besar" << endl;
    cout << "Huruf yang anda tekan, Huruf " << A << endl;
  } else
  {
    cout << "Anda tidak menekan huruf besar" << endl;
    cout << "Karakter yang anda tekan, Karakter " << A << endl;
  }

  return 0;
}